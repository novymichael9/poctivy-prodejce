<section class="angle-bg sky-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Stránka nenalezena :(</h2>
                <p>Stránku kterou chcete najít neexistuje.</p>
            </div>
        </div>
    </div>
</section>
<!--Feature-area-->
<section class="gray-bg section-padding" id="service-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="box">
                    <div class="box-icon">
                        <img src="images/service-icon-1.png" alt="">
                    </div>
                    <h4>LEHKÉ K POCHOPENÍ</h4>
                    <p>Tento projekt pochopíte hned, je udělaný co nejléhce, aby se nikdo v tom neztratil.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="box">
                    <div class="box-icon">
                        <img src="images/service-icon-2.png" alt="">
                    </div>
                    <h4>SUPER DESIGN</h4>
                    <p>Všechno je udělané v nejmodernějších technologií, aby to bylo co nejhezčí a zapůsobilo dojem.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Feature-area/-->

<section class="angle-bg sky-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="caption_slide" class="carousel slide caption-slider" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active row">
                            <div class="v-center">
                                <div class="col-xs-12 col-md-6">
                                    <div class="caption-title" data-animation="animated fadeInUp">
                                        <h2>Z pohledu prodávající osoby</h2>
                                    </div>
                                    <div class="caption-desc" data-animation="animated fadeInUp">
                                        <p>Když něco někdo někomu prodá, hned je z něho prodávájící osoba, jestli
                                            prodává poprvé a nezná nikoho, budou mu ostatní těžce věřit, proto
                                            jestli se ti podaří něco prodat, založ si účet na této stránce a požádej
                                            kupce o to aby ti zrencenzoval obchod s tebou, už bude na něm co ti do
                                            recenze napíše.</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo one" data-animation="animated fadeInRight">
                                        <img src="images/selling.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item row">
                            <div class="v-center">
                                <div class="col-xs-12 col-md-6">
                                    <div class="caption-title" data-animation="animated fadeInUp">
                                        <h2>Z pohledu nákupující osoby</h2>
                                    </div>
                                    <div class="caption-desc" data-animation="animated fadeInUp">
                                        <p>Jestli toho dotyčného prodejce neznáš, můžeš otevřít seznam prodejců a
                                            podívat se zda někdo takový je na této stránce a jestli jo, podívej se
                                            rovnou na hodnocení zda mu můžeš důvěřovat.</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo one" data-animation="animated fadeInRight">
                                        <img src="images/buying.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Indicators -->
                    <ol class="carousel-indicators caption-indector">
                        <li data-target="#caption_slide" data-slide-to="0" class="active">
                            <strong>Z pohledu</strong>prodávájící osoby
                        </li>
                        <li data-target="#caption_slide" data-slide-to="1">
                            <strong>Z pohledu </strong>nakupující osoby
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="gray-bg section-padding" id="faq-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                <div class="page-title">
                    <h2>Nejčastější otázky</h2>
                    <p>Jestli jsi zde nenašel tvoji otázku, neboj se zeptat!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <div class="panel-group" id="accordion">
                    <div class="panel">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">Kdy
                                to bude kompletně hotové?</a>
                        </h4>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <p>To netuším, dávám do toho hodně času, ale zatím všechno jde pomalu, aby to bylo stabilní
                                a bezpečné.</p>
                        </div>
                    </div>
                    <div class="panel">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Bude to zdarma?</a>
                        </h4>
                        <div id="collapse2" class="panel-collapse collapse">
                            <p>Ano, bude. Maximálně si budete moct koupit později propagaci prodejní Facebook
                                skupiny.</p>
                        </div>
                    </div>
                    <div class="panel">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Bude se to moct využít
                                tak, aby to bylo k něčemu platné?</a>
                        </h4>
                        <div id="collapse3" class="panel-collapse collapse">
                            <p>To závisí na komunitě jestli budou mít zájem nebo ne.</p>
                        </div>
                    </div>
                    <div class="panel">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Kde to uvidím
                                poprvé?</a>
                        </h4>
                        <div id="collapse4" class="panel-collapse collapse">
                            <p>Chtěl bych tento projekt rozšířit po všech CZ/SK prodejních Facebookových skupinách. Ze
                                začátku bych to chtěl zkusit rozšířit mezi
                                aukce o kvalitní oblečení zda se to uchytí, jestli se to uchytí, budu postupně to
                                přidávat do dalších skupin, do jakých to nevím.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$uzivatele = Db::queryAll('
    SELECT *
    FROM users
    ORDER BY users_id DESC');

$query = $_GET['search-it'];


if (isset($query)) {
    if(strlen($query) >= 3) {
        $result = Db::query("SELECT * FROM users
            WHERE (`firstname` LIKE '%".$query."%') OR (`lastname` LIKE '%".$query."%')");

    }
}

?>

<!--Feature-area-->
<section class="gray-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Seznam uživatelů</h2>
            </div>
            <div class="col-12">
                <form method="get">
                    <input type="text" name="search" placeholder="Napiš jméno uživatele" class="form-control" required="required">
                    <input type="submit" name="search-it" class="button" value="Hledat">
                </form>
            </div>
            <div class="col-12">
                <table border="1" class="table">
                    <tr>
                        <th>Jméno</th>
                        <th>Příjmení</th>
                        <th>Přezdívka</th>
                        <th>Počet hodnocení</th>
                        <th>Průměrné hodnocení</th>
                    </tr>
                    <?php
                    foreach ($uzivatele as $uzivatel) {
                        $link = htmlspecialchars($uzivatel['username']);
                        $id = htmlspecialchars($uzivatel['users_id']);
                        $linktoprofile = '<a href="/?p=profile&id=' . htmlspecialchars($uzivatel['users_id']) . '">' . $uzivatel['username'] . '</a>';
                        echo("<tr><td>" . htmlspecialchars($uzivatel['firstname']) . "</td><td>" . htmlspecialchars($uzivatel['lastname']) . "</td><td>" . $linktoprofile . "</td><td>" . htmlspecialchars($uzivatel['quantity_reviews']) . "</td><td>5.5</td></tr>");
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</section>
<!--Feature-area/-->
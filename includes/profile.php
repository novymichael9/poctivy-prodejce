<?php

/* TODO : Following systém když ho odebírá tak zobrazit tlačítko zrušit odběr */

if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    header('Location: /?p=404');
}

$profile = Db::queryOne('
    SELECT *
    FROM users
    WHERE users_id=?
    ', $id);
$facebookprofile = '<a href="' . htmlspecialchars($profile['facebook_profile']) . '" target="_blank">odkaz zde</a>';

$profile_session = Db::queryOne('
                SELECT firstname, lastname
                FROM users
                WHERE users_id=?
        ', $_SESSION['user_id']);

$_SESSION['user_firstname'] = $profile_session['firstname'];
$_SESSION['user_lastname'] = $profile_session['lastname'];


$reviews_extract = Db::queryAll('
    SELECT *
    FROM reviews
    WHERE users_id=?
    ', $id);

if (isset($_SESSION['user_id'])) {
    if (isset($_POST['accept_terms_add_review'])) {
        if (isset($_POST['add_review'])) {
            Db::query('
                    INSERT INTO reviews (description, score, username, users_id)
                    VALUES (?, ?, ?, ?)
                    ', $_POST['description_add_review'], $_POST['stars_add_review'], $_SESSION['user_username'], $id);

            Db::query('
                UPDATE users
                SET quantity_reviews= quantity_reviews + 1
                WHERE users_id=?
                ', $id);

            $message_add_review = 'Recenze byla úspěšně vložena.';
        }
    } else {
        $message_add_review = 'Pro zveřejnění recenze musíte potvrdit podmínky.';
    }

} else {
    $message_add_review = 'Pro zveřejnení recenze se musíte přihlásit.';
}

if (isset($_POST['submit-product'])) {

    $date = date("Y.m.d");

    Db::query('
    INSERT INTO products (users_id, name, date, price, link, kind)
    VALUES (?, ?, ?, ?, ?, ?)
    ', $id, $_POST['name-product'], $date, $_POST['price-product'], $_POST['link-product'], $_POST['kind-product']);

    $message_add_product = "Produkt byl vložen";
}

$products = Db::queryAll('
    SELECT *
    FROM products
    WHERE users_id=?
    ', $id);

if (isset($_POST['follow-add'])) {
    Db::query('
    INSERT INTO followed_users (users_id, followed_users_id)
    VALUES (?, ?)
    ', $_SESSION['user_id'], $_GET['id']);
}

$following = Db::query('
    SELECT followed_users_id
    FROM followed_users
    WHERE users_id=?
    ', $_SESSION['user_id']);

?>
<section class="gray-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="profile">
                <div class="col-12">
                    <h2>Profil uživatele : <?= htmlspecialchars($profile['username']) ?></h2>
                </div>
                <div class="col-12">
                    <img src="../images/version.png" alt="Uživatel" class="user-image">
                </div>
                <div class="col-12 text-center">
                    <form method="post">
                        <?php
                        if ($_SESSION['user_id'] != $id) {
                            if ($id == $following) {
                                ?>
                                <input type="submit" value="Zrušit sledování" name="follow-discard" class="button">
                            <?php } ?>
                            <input type="submit" value="Sledovat" name="follow-add" class="button">
                        <?php } ?>
                    </form>


                </div>
                <div class="col-12">
                    <br/>
                </div>
                <div class="col-12 text-center">
                    <?php
                    if ($_SESSION['user_id'] == $_GET['id']) {
                        $linktosettings = '<a href="/?p=settings-profile&id=' . htmlspecialchars($id) . '" class="button">Upravit profil</a>';
                        echo($linktosettings);
                    }
                    ?>
                    <a href="#add_review" data-toggle="modal" data-target="#add_review" class="button">Přidat
                        hodnocení</a>
                </div>
                <div class="col-12 text-center">
                    <h2>Informace o profilu</h2>
                    <p>ID profilu : <?= htmlspecialchars($profile['users_id']) ?></p>
                    <p>Celé jméno
                        : <?= htmlspecialchars($profile['firstname']) . " " . htmlspecialchars($profile['lastname']) ?></p>
                    <p>Facebookový profil : <?php echo($facebookprofile); ?></p>
                    <?php
                    if ($profile['sw_city'] == 1) {
                        ?>
                        <p>Město : <?= htmlspecialchars($profile['city']) ?></p>
                        <?php
                    } else {
                        ?>
                        <p>Město : <i>skryté</i></p>
                        <?php
                    }
                    if ($profile['sw_age'] == 1) {
                        ?>
                        <p>Věk : <?= htmlspecialchars($profile['age']) ?></p>
                        <?php
                    } else {
                        ?>
                        <p>Věk : <i>skryté</i></p>
                        <?php
                    }
                    if ($profile['sw_email'] == 1) {
                        ?>
                        <p>Email : <?= htmlspecialchars($profile['email']) ?></p>
                        <?php
                    } else {
                        ?>
                        <p>Email : <i>skryté</i></p>
                        <?php
                    }

                    if ($profile['admin'] == 1) {
                        echo('<p>Role : <strong>Admin</strong></p>');
                    } else {
                        echo('<p>Role : Člen');
                    }
                    ?>
                </div>
                <div class="col-12 text-center">
                    <h2>Facebookové skupiny, kde prodávám</h2>
                    <?php
                    echo('<a href="' . htmlspecialchars($profile['facebook_group1_link']) . '" target="_blank">' . htmlspecialchars($profile['facebook_group1_name']) . '</a><br />');
                    echo('<a href="' . htmlspecialchars($profile['facebook_group2_link']) . '" target="_blank">' . htmlspecialchars($profile['facebook_group2_name']) . '</a><br />');
                    echo('<a href="' . htmlspecialchars($profile['facebook_group3_link']) . '" target="_blank">' . htmlspecialchars($profile['facebook_group3_name']) . '</a><br />');
                    echo('<a href="' . htmlspecialchars($profile['facebook_group4_link']) . '" target="_blank">' . htmlspecialchars($profile['facebook_group4_name']) . '</a><br />');
                    echo('<a href="' . htmlspecialchars($profile['facebook_group5_link']) . '" target="_blank">' . htmlspecialchars($profile['facebook_group5_name']) . '</a><br />');;
                    ?>
                </div>
                <div class="col-12 text-center">
                    <h2>Moje aktuální nabídky</h2>
                    <?php
                    if ($_SESSION['user_id'] == $_GET['id']) {
                        ?>
                        <a href="#add_product" data-toggle="modal" data-target="#add_product" class="button">Přidat
                            produkt</a>
                        <?php
                    }
                    ?>
                    <?php
                    /*
                    if ($_SESSION['user_id'] == $_GET['id']) {
                        ?>
                        <a href="#" class="button">Odebrat</a>
                        <a href="#" class="button">Upravit</a>
                        <?php
                    } */
                    ?>
                    <table border="1" class="table">
                        <tr>
                            <th>Název produktu</th>
                            <th>Datum přidání (Rok - měsíc - den)</th>
                            <th>Cena</th>
                            <th>Odkaz na prodej</th>
                            <th>Druh produktu</th>
                        </tr>
                        <?php
                        foreach ($products as $product) {
                            echo('<tr><td>' . $product['name'] . '</td><td>' . $product['date'] . '</td><td>' . $product['price'] . '</td><td>' . $product['link'] . '</td><td>' . $product['kind'] . '</td></tr>');
                        }
                        ?>
                    </table>

                </div>
                <div class="col-12 text-center">
                    <h2>Recenze na mě</h2>
                    <?php
                    foreach ($reviews_extract as $reviews) {
                        $linktoprofile = '<a href="/?p=profile&id=' . htmlspecialchars($reviews['users_id']) . '">' . $reviews['username'] . '</a>';
                        echo('<div class="review_list">' . $linktoprofile . '<p>Hodnocení : ' . $reviews['score'] . ' / 5</p><p>' . $reviews['description'] . '</p></div>');
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Feature-area/-->

<!-- modal na profilu -->

<div class="modal fade add_review" tabindex="-1" role="dialog" aria-labelledby="add_review" aria-hidden="true"
     id="add_review">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Přidat hodnocení</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h2>Přidat hodnocení</h2>
                <?php
                if (isset($message_add_review)) {
                    echo('<p>' . $message_add_review . '</p>');
                }
                ?>
                <?php
                if (empty($_SESSION['user_id'])) {
                    ?>
                    <p>Pro přidání recenze se musíte zde přihlásit, pokud zde nemáte účet, musíte se zaregistrovat.</p>
                    <a href="#" class="button">Přihlásit se</a>
                    <a href="#" class="button">Registrovat se</a>
                    <br/>
                    <br/>
                    <?php
                }
                ?>
                <form method="post">
                    <?php
                    if (isset($_SESSION['user_id'])) {
                        echo('<p>Jméno : ' . $_SESSION['user_firstname'] . " " . $_SESSION['user_lastname'] . '</p>');
                    }
                    ?>
                    <input type="number" name="stars_add_review" placeholder="Bodové hodnocení 0-5" min="0" max="5"
                           required="required">
                    <textarea placeholder="Zhodnocení obchodu" required="required" name="description_add_review"
                              rows="4"></textarea>
                    <input type="checkbox" name="accept_terms_add_review"> Souhlasím s zveřejňováním mých údajů na této
                    stránce.<br/><br/>
                    <input type="submit" value="Zveřejnit" class="button" name="add_review">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Zavřít</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade add_product" tabindex="-1" role="dialog" aria-labelledby="add_product" aria-hidden="true"
     id="add_product">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Přidat produkt</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h2>Přidat produkt</h2>
                <?php
                if (isset($message_add_product)) {
                    echo('<p>' . $message_add_product . '</p>');
                }
                ?>
                <form method="post">
                    <input type="text" name="name-product" placeholder="Název produktu" required="required">
                    <input type="text" name="price-product" placeholder="Cena v Kč" required="required">
                    <input type="text" name="link-product" placeholder="Odkaz na produkt" required="required">
                    <input type="text" name="kind-product" placeholder="Druh produktu" required="required">
                    <input type="submit" name="submit-product" value="Přidat produkt" class="button">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Zavřít</button>
            </div>
        </div>
    </div>
</div>
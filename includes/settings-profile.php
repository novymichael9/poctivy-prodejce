<?php

if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    header('Location: /?p=404');
}

if ($_SESSION['user_id'] != $_GET['id']) {
    die('Nedostatečná oprávnění.');
}

if (isset($_POST['update-confirm'])) {
    Db::query('
        UPDATE users
        SET username=?, email=?, firstname=?, lastname=?, facebook_profile=?, city=?, age=?, facebook_group1_link=?, facebook_group2_link=?, facebook_group3_link=?, facebook_group4_link=?, facebook_group5_link=?, facebook_group1_name=?, facebook_group2_name=?, facebook_group3_name=?, facebook_group4_name=?, facebook_group5_name=?
        WHERE users_id=?
        ', $_POST['update-nickname'], $_POST['update-email'], $_POST['update-firstname'], $_POST['update-lastname'],
        $_POST['update-facebook-profile'], $_POST['update-city'], $_POST['update-age'],
        $_POST['update-facebook-group1'], $_POST['update-facebook-group2'], $_POST['update-facebook-group3'],
        $_POST['update-facebook-group4'], $_POST['update-facebook-group5'], $_POST['update-facebook-group1-name'],
        $_POST['update-facebook-group2-name'], $_POST['update-facebook-group3-name'],
        $_POST['update-facebook-group4-name'], $_POST['update-facebook-group5-name'], $id);
    $message_update = 'Profil byl upraven.';
}

/* if(isset($_POST['update-confirm-password'])) {

    $message_update = 'Heslo bylo změněno.';
} */

if (isset($_POST['accept_settings'])) {
    if (isset($_POST['show_city'])) {
        Db::query('
        UPDATE users
        SET sw_city=1
        WHERE users_id=?
        ', $id);
    } else {
        Db::query('
        UPDATE users
        SET sw_city=0
       WHERE users_id=?
       ', $id);
    }

    if (isset($_POST['show_email'])) {
        Db::query('
        UPDATE users
        SET sw_email=1
        WHERE users_id=?
        ', $id);
    } else {
        Db::query('
        UPDATE users
        SET sw_email=0
        WHERE users_id=?
        ', $id);
    }

    if (isset($_POST['show_age'])) {
        Db::query('
        UPDATE users
        SET sw_age=1
        WHERE users_id=?
        ', $id);
    } else {
        Db::query('
        UPDATE users
        SET sw_age=0
        WHERE users_id=?
        ', $id);
    }

    if(isset($_POST['password_new'])) {
        $password = password_hash($_POST['password_new'], PASSWORD_DEFAULT);
        Db::query('
        UPDATE users
        SET password=?
        WHERE users_id=?
        ', $password, $id);
    }

    $message_update = 'Vaše nastavení bylo uložené.';
}

$profile = Db::queryOne('
    SELECT *
    FROM users
    WHERE users_id=?
    ', $id);
?>
<section class="gray-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="profile">
                <div class="col-12">
                    <h2>Profil uživatele : <?= htmlspecialchars($profile['username']) ?></h2>
                </div>
                <div class="col-12">
                    <img src="../images/version.png" alt="Uživatel" class="user-image">
                </div>
                <div class="col-12 text-center">

                </div>
                <div class="col-12 text-center">
                    <h2>Informace o profilu</h2>
                    <?php
                    if (isset($message_update)) {
                        echo('<p>' . $message_update . '</p>');
                    }
                    ?>
                    <a href="#settings" data-toggle="modal" data-target="#settings" class="button">Nastavení profilu</a>
                    <p>ID profilu : <?= htmlspecialchars($profile['users_id']) ?></p>
                    <form method="post">
                        <input type="hidden" name="update-id" value="<?= htmlspecialchars($profile['users_id']) ?>"/>
                        <input type="text" class="form-control" name="update-nickname"
                               value="<?= htmlspecialchars($profile['username']) ?>">
                        <input type="text" class="form-control" name="update-email"
                               value="<?= htmlspecialchars($profile['email']) ?>">
                        <input type="text" class="form-control" name="update-firstname"
                               value="<?= htmlspecialchars($profile['firstname']) ?>">
                        <input type="text" class="form-control" name="update-lastname"
                               value="<?= htmlspecialchars($profile['lastname']) ?>">
                        <input type="text" class="form-control" name="update-facebook-profile"
                               value="<?= htmlspecialchars($profile['facebook_profile']) ?>">
                        <input type="text" class="form-control" name="update-city"
                               value="<?= htmlspecialchars($profile['city']) ?>">
                        <input type="text" class="form-control" name="update-age"
                               value="<?= htmlspecialchars($profile['age']) ?>">
                        <br/>
                        <h4>5 Facebookových skupin kde prodáváš nejčasteji</h4>
                        <p>Vzor : https://www.facebook.com/groups/1737494903204939/</p>
                        <br/>
                        <input type="text" class="form-control" name="update-facebook-group1-name"
                               value="<?= htmlspecialchars($profile['facebook_group1_name']) ?>"
                               placeholder="Jméno Facebook skupiny">
                        <input type="text" class="form-control" name="update-facebook-group1"
                               value="<?= htmlspecialchars($profile['facebook_group1_link']) ?>"
                               placeholder="Link Facebook skupiny"><br/>
                        <input type="text" class="form-control" name="update-facebook-group2-name"
                               value="<?= htmlspecialchars($profile['facebook_group2_name']) ?>"
                               placeholder="Jméno Facebook skupiny">
                        <input type="text" class="form-control" name="update-facebook-group2"
                               value="<?= htmlspecialchars($profile['facebook_group2_link']) ?>"
                               placeholder="Link Facebook skupiny"><br/>
                        <input type="text" class="form-control" name="update-facebook-group3-name"
                               value="<?= htmlspecialchars($profile['facebook_group3_name']) ?>"
                               placeholder="Jméno Facebook skupiny"">
                        <input type="text" class="form-control" name="update-facebook-group3"
                               value="<?= htmlspecialchars($profile['facebook_group3_link']) ?>"
                               placeholder="Link Facebook skupiny"><br/>
                        <input type="text" class="form-control" name="update-facebook-group4-name"
                               value="<?= htmlspecialchars($profile['facebook_group4_name']) ?>"
                               placeholder="Jméno Facebook skupiny"">
                        <input type="text" class="form-control" name="update-facebook-group4"
                               value="<?= htmlspecialchars($profile['facebook_group4_link']) ?>"
                               placeholder="Link Facebook skupiny"><br/>
                        <input type="text" class="form-control" name="update-facebook-group5-name"
                               value="<?= htmlspecialchars($profile['facebook_group5_name']) ?>"
                               placeholder="Jméno Facebook skupiny"">
                        <input type="text" class="form-control" name="update-facebook-group5"
                               value="<?= htmlspecialchars($profile['facebook_group5_link']) ?>"
                               placeholder="Link Facebook skupiny">
                        <input type="submit" value="Potvrdit změny" name="update-confirm" class="button">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Feature-area/-->

<!-- MODALY -->

<div class="modal fade settings" tabindex="-1" role="dialog" aria-labelledby="settings" aria-hidden="true"
     id="settings">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrace</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h2>Nastavení profilu</h2>
                <?php
                if (isset($message_settings)) {
                    echo('<p>' . $message_settings . '</p>');
                }
                ?>
                <form method="post">
                    <?php
                    if ($profile['sw_city'] == 1) {
                        ?>
                        <input type="checkbox" name="show_city" checked="checked"> Zobrazit město<br/>
                    <?php } else { ?>
                        <input type="checkbox" name="show_city"> Zobrazit město<br/>
                        <?php
                    }
                    if ($profile['sw_email'] == 1) {
                        ?>
                        <input type="checkbox" name="show_email" checked="checked"> Zobrazit email<br/>
                        <?php
                    } else {
                        ?>
                        <input type="checkbox" name="show_email"> Zobrazit email<br/>
                        <?php
                    }
                    if ($profile['sw_age'] == 1) {
                        ?>
                        <input type="checkbox" name="show_age" checked="checked"> Zobrazit věk<br/>
                        <?php
                    } else {
                    ?>
                    <input type="checkbox" name="show_age"> Zobrazit věk<br/>
                    <?php } ?>
                    <br />
                    <input type="password" name="password_new" class="form-control" placeholder="Vaše nové heslo">
                    <br />
                    <input type="submit" value="Potvrdit" name="accept_settings" class="button">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Zavřít</button>
            </div>
        </div>
    </div>
</div>
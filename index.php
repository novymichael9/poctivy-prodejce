<?php
require_once('./app/init.php');

/* TODO: Chybí poslat mail a změnit heslo, modal k změně hesla už funguje. */


if (isset($_POST['register-check'])) {
    if (isset($_POST['register'])) {
   if ($_POST['register-password'] != $_POST['register-password_repeat']) {
        $message = "Hesla nejsou stejná.";
    } else {
        $exist = Db::querySingle('
                        SELECT COUNT(*)
                        FROM users
                        WHERE username=?
                        LIMIT 1
                ', $_POST['register-username']);
        if ($exist) {
            $message = "Uživatel s touto přezdívkou nebo emailem existuje.";
        } else {
            $password = password_hash($_POST['register-password'], PASSWORD_DEFAULT);
            Db::query('
                INSERT INTO users (username, password, firstname, lastname, email, facebook_profile, city, age)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?)
                ', $_POST['register-username'], $password, $_POST['register-firstname'], $_POST['register-lastname'], $_POST['register-email'], $_POST['register-facebook'], $_POST['register-city'], $_POST['register-age']);
            $_SESSION['user_id'] = Db::getLastId();
            $_SESSION['user_username'] = $_POST['register-username'];
            $_SESSION['user_admin'] = 0;
            header('Location: /?p=homepage');
            exit();
        }
    }
}
} else {
    $message_register = "Nesouhlasili jste s podmínkami";
}


if (isset($_POST['login']))
{
    $userlog = Db::queryOne('
                SELECT users_id, admin, password
                FROM users
                WHERE username=?
        ', $_POST['login-username']);
    if (!$userlog || !password_verify($_POST['login-password'], $userlog['password']))
        $message = 'Neplatné uživatelské jméno nebo heslo';
    else
    {
        $_SESSION['user_id'] = $userlog['users_id'];
        $_SESSION['user_username'] = $_POST['login-username'];
        $_SESSION['user_admin'] = $userlog['admin'];
        header('Location: /');
        exit();
    }
}

$hlaska = '';
if (isset($_POST)) {
    if (isset($_POST['form-name']) && $_POST['form-name'] &&
        isset($_POST['form-email']) && $_POST['form-email'] &&
        isset($_POST['form-antispam']) && $_POST['form-antispam'] == (4)) {
        $hlavicka = 'From:' . $_POST['form-email'];
        $hlavicka .= "\nMIME-Version: 1.0\n";
        $hlavicka .= "Content-Type: text/html; charset=\"utf-8\"\n";
        $adresa = 'michael.novy@speedfire.cz';
        $predmet = 'Nová zpráva z poctivyprodejce.wz.cz';
        $email = $_POST['form-email'];
        $jmeno = $_POST['form-name'];
        $zprava = $_POST['message'];
        $message = "Jméno : " . $jmeno . "<br /><br />" . "Email : " . $email . "<br /><br />" . "Zpráva : " . $zprava;
        $uspech = mb_send_mail($adresa, $predmet, $message, $hlavicka);
        if ($uspech) {
            $hlaska = 'Email byl úspěšně odeslán, brzy vám odpovíme.';
        } else {
            $hlaska = 'Email se nepodařilo odeslat. Zkontrolujte adresu.';
        }
    } elseif (isset($_POST['form-antispam']) && $_POST['form-antispam'] != (4)) {
        $hlaska = 'Špatně vyplněný antispam.';
    } else {
        $hlaska = 'Formulář je špatně vyplněný, zkontrolujte jméno a email, popřípadě antispam';
    }
}



if (isset($_POST['forgot'])) {
    $forgot_mail_check = Db::querySingle('
                        SELECT COUNT(*)
                        FROM users
                        WHERE email=?
                        LIMIT 1
                ', $_POST['forgot-email']);
    if($forgot_mail_check) {
        header('Location: /?p=404');
    } else {
        $message_forgot = "Neeixstuje";
    }
}

if(isset($_GET['logout'])) {
    session_destroy();
    header('Location: /?p=homepage');
    exit();
}




?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="Michael Nový">
    <meta name="description" content="">
    <meta name="keywords"
          content="Poctivyprodejce, Facebook, Prodejni skupiny, Hodnoceni, uzivatele, prodej, nakup, scammerlist, scammer">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>Poctivý Prodejce</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/>
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/animate.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target="#primary-menu">

<div class="preloader">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
<!--Mainmenu-area-->
<div class="mainmenu-area" data-spy="affix" data-offset-top="100">
    <div class="container">
        <!--Logo-->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand logo">
                <h2>Poctivý Prodejce</h2>
            </a>
        </div>
        <!--Logo/-->
        <nav class="collapse navbar-collapse" id="primary-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="/">Domů</a></li>
                <?php if ($_SERVER["REQUEST_URI"] == '/?p=homepage' || $_SERVER['REQUEST_URI'] == '/') { ?>
                <li><a href="#service-page">Použití</a></li>
                <li><a href="#faq-page">FAQ</a></li>
                <?php } ?>
                <li><a href="#contact-page">Kontakt</a></li>
                <?php
                if (isset($_SESSION['user_id'])) { ?>
                    <li><a href="/?p=list-of-users">Seznam uživatelů</a></li>
                    <li><?php echo('<a href="/?p=profile&id=' . htmlspecialchars($_SESSION['user_id']) . '">' . ($_SESSION['user_username']) . '</a>') ?></li>
                    <li><a href="?logout">Odhlásit se</a></li>
                    <?php
                } else {
                    ?>
                    <li><a href="#login" data-toggle="modal" data-target="#login">Přihlášení</a></li>
                    <li><a href="#registration" data-toggle="modal" data-target="#registration">Registrace</a></li>
                <?php } ?>
            </ul>
        </nav>
    </div>
</div>
<!--Mainmenu-area/-->


<!--Header-area-->
<header class="header-area overlay full-height relative v-center" id="home-page">
    <div class="absolute anlge-bg"></div>
    <div class="container">
        <div class="row v-center">
            <div class="col-xs-12 col-md-7 header-text">
                <h2>Poctivý Prodejce</h2>
            </div>
        </div>
    </div>
</header>
<!--Header-area/-->

<?php
$page = 'homepage';
if (isset($_GET['p']) && !empty($_GET['p'])) {
    $page = $_GET['p'];
}
if (file_exists("./includes/{$page}.php")) {
    include_once("./includes/{$page}.php");
} else {
    include_once("./includes/404.php");
}
?>

<footer class="footer-area relative sky-bg" id="contact-page">
    <div class="absolute footer-bg"></div>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Kontaktujte mě</h2>
                        <p>Chcete se na něco zeptat? Chcete spolupracovat ? Neváhejte a pište, odepíšu vám co
                            nejrychleji.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php
                    echo("<p>" . $hlaska . "</p>");
                    ?>
                    <form id="contact-form" method="post" class="contact-form">
                        <div class="form-double">
                            <input type="text" id="form-name" name="form-name" placeholder="Vaše jméno"
                                   class="form-control" required="required">
                            <input type="email" id="form-email" name="form-email" class="form-control"
                                   placeholder="Váš email" required="required">
                        </div>
                        <input type="text" id="form-subject" name="form-antispam" class="form-control"
                               placeholder="Kolik je 2+2?">
                        <textarea name="message" id="form-message" rows="5" class="form-control"
                                  placeholder="Vaše zpráva" required="required"></textarea>
                        <input type="submit" class="button" value="Odeslat">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 pull-right">
                    <ul class="social-menu text-right x-left">
                        <li><a href="https://www.facebook.com/michaelnovy.novy"><i class="ti-facebook"></i></a></li>
                        <li><a href="https://twitter.com/mickley_new"><i class="ti-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/novymichael"><i class="ti-instagram"></i></a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <p>Můžete mě kontaktovat i pomocí sociálních sítí.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p>&copy;Vytvořil <a href="http://novymichael.wz.cz">Nový Michael</a> 2018</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Modaly -->

<div class="modal fade login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true" id="login">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Přihlášení</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h2>Přihlášení</h2>
                <?php
                if (isset($message)) {
                    echo('<p>' . $message . '</p>');
                }
                ?>
                <form method="post">
                    <input type="text" name="login-username" placeholder="Přezdívka" required="required">
                    <input type="password" name="login-password" placeholder="Heslo" required="required">
                    <input type="submit" value="Přihlásit se" class="button" name="login">
                </form>
                <a href=#password" data-toggle="modal" data-target="#password">Zapomněli jste heslo ?</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Zavřít</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade registration" tabindex="-1" role="dialog" aria-labelledby="registration" aria-hidden="true"
     id="registration">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrace</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h2>Registrace</h2>
                <?php
                if (isset($message_register)) {
                    echo('<p>' . $message_register . '</p>');
                }
                ?>
                <p>Vše co je označené <strong>*</strong> je povinné!</p>
                <form method="post">
                    <input type="text" name="register-username" placeholder="Přezdívka *" required="required">
                    <input type="email" name="register-email" placeholder="Email *" required="required">
                    <input type="text" name="register-firstname" placeholder="Jméno *" required="required">
                    <input type="text" name="register-lastname" placeholder="Příjmení *" required="required">
                    <input type="text" name="register-facebook" placeholder="Link na facebook profil * (https://www.face..)" required="required">
                    <input type="text" name="register-city" placeholder="Město vašeho bydliště *" required="required">
                    <input type="text" name="register-age" placeholder="Věk *" required="required">
                    <input type="password" name="register-password" placeholder="Heslo *" required="required">
                    <input type="password" name="register-password_repeat" placeholder="Heslo znovu *"
                           required="required">
                    <input type="checkbox" name="register-check">Souhlasím s zveřejňováním mých údajů na této stránce.<br>
                    <input type="submit" value="Registrovat se" class="button" name="register">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Zavřít</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade password" tabindex="-1" role="dialog" aria-labelledby="password" aria-hidden="true" id="password">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Obnovení hesla</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h2>Obnovit heslo</h2>
                <?php
                if (isset($message)) {
                    echo('<p>' . $message_forgot . '</p>');
                }
                ?>
                <form method="post">
                    <input type="email" name="forgot-email" placeholder="Email" required="required">
                    <input type="submit" value="Obnovit heslo" class="button" name="forgot">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Zavřít</button>
            </div>
        </div>
    </div>
</div>


<!--Vendor-JS-->
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<!--Plugin-JS-->
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/scrollUp.min.js"></script>
<script src="js/magnific-popup.min.js"></script>
<script src="js/wow.min.js"></script>
<!--Main-active-JS-->
<script src="js/main.js"></script>
</body>

</html>
